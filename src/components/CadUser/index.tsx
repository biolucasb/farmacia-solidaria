import React, { useState } from 'react';
import { Form, Input, Select, Button, Row, Col, Modal } from 'antd';
import './style.css';
const { Option } = Select;

const RegistroUser: React.FC = () => {
    const [form] = Form.useForm();
    const [passwordError, setPasswordError] = useState("");
    const [usuarioVisivel, setusuarioVisivel] = useState(false);

    const handleSubmit = () => {
      form
        .validateFields()
        .then((values) => {
          if (values.password !== values.confirmPassword) {
            setPasswordError("As senhas não coincidem");
          } else {
            setPasswordError("");
            alert("Cadastro realizado!");
            // Adicione aqui o código para enviar os dados do formulário
          }
        })
        .catch((info) => {
          console.log("Validação falhou:", info);
        });
    };

    const handleCadastrarClienteClick = () => {
        console.log('Cadastro de usuário realizado');
        form.submit();
    };

    const handleCancelarClienteClick = () => {
        setusuarioVisivel(false);
    };

    return (
        <>
            {/* Modal para Cadastrar Usuarios */}
            <div className='pt-2'>
                <Modal
                    title='Cadastrar Usuário'
                    visible={usuarioVisivel}
                    onOk={handleCadastrarClienteClick}
                    onCancel={handleCancelarClienteClick}
                >
                    <Form form={form} layout="vertical" onFinish={handleSubmit}>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item
                                    label="Nome"
                                    name="name"
                                    rules={[{ required: true, message: 'Por favor adicione o nome do novo usuário.' }]}
                                >
                                    <Input placeholder="Nome do novo usuário" />
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    label="CPF"
                                    name="cpf"
                                    rules={[{ required: true, message: 'Por favor adicione o CPF.' }]}
                                >
                                    <Input placeholder="Informe o CPF do usuário" />
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    label="Telefone"
                                    name="phone"
                                    rules={[{ required: true, message: 'Por favor informe um telefone para contato.' }]}
                                >
                                    <Input placeholder="(00) 0 0000-0000" />
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    label="Senha"
                                    name="password"
                                    rules={[{ required: true, message: 'Por favor adicione uma senha para dar sequência.' }]}
                                >
                                    <Input.Password placeholder="Informe uma senha segura." />
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    label="Confirme sua Senha"
                                    name="confirmPassword"
                                    rules={[{ required: true, message: 'Por favor confirme sua senha.' }]}
                                >
                                    <Input.Password placeholder="Confirme sua senha" />
                                </Form.Item>
                            </Col>
                            {passwordError && (
                                <Col span={24}>
                                    <p style={{ color: 'red', marginTop: '10px' }}>{passwordError}</p>
                                </Col>
                            )}
                            <Col span={24}>
                                <Form.Item
                                    label="Função"
                                    name="role"
                                    rules={[{ required: true, message: 'Por favor selecione uma função.' }]}
                                >
                                    <Select placeholder="Selecione uma Função">
                                        <Option value="Admin">Admin</Option>
                                        <Option value="Usuário">Usuário</Option>
                                    </Select>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
                <Button className="button w-100" onClick={() => setusuarioVisivel(true)}>Novo Funcionário</Button>
            </div>
        </>
    );
};

export default RegistroUser;
