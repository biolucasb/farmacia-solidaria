import React from "react";
import { FaInstagram, FaWhatsapp, FaGithub } from "react-icons/fa";
import { Layout, Row, Col } from 'antd';
import Logo from "../../assets/logo.png";
import './style.css';

const { Footer } = Layout;

const ComponentFooter: React.FC = () => {
    return (
        <Footer className="pt-4">
            <Row>
                <Col span={12} className="txt-center">
                    <img className="w-50" src={Logo} alt="" />
                </Col>
                <Col span={12}>
                    <div className="txt-center">
                        <h5>Visite nossas redes sociais</h5>
                    </div>
                    <div className="txt-center">
                        <ul className="d-flex txt-center" style={{ listStyleType: "none", padding: 0 }}>
                            <li className="iconT" style={{ display: "inline-block", marginRight: "16px" }}>
                                <a target='_blank' href="#"><FaInstagram /></a>
                            </li>
                            <li className="iconT" style={{ display: "inline-block", marginRight: "16px" }}>
                                <a target='_blank' href="#"><FaWhatsapp /></a>
                            </li>
                            <li className="iconT" style={{ display: "inline-block" }}>
                                <a target='_blank' href="#"><FaGithub /></a>
                            </li>
                        </ul>
                    </div>
                </Col>
            </Row>
        </Footer>
    );
}

export default ComponentFooter;
