import React, { useState } from 'react';
import { Button, Col, DatePicker, Form, Input, Row, message } from 'antd';
import './style.css';
import ModalPadrao from '@components/ModalPadrao';
import validateCPF from '@utils/validate-cpf';
import { MaskedInput } from 'antd-mask-input';
import { buscarCEP } from './services/cepService';

const FormCliente: React.FC = () => {
    const [form] = Form.useForm();
    const [clienteVisivel, setClienteVisivel] = useState(false);

    const handleSubmit = async () => {
        try {
            const values = await form.validateFields();
            console.log('Received values:', values);
            // Adicione aqui o código para enviar os dados do formulário
        } catch (errorInfo) {
            console.log('Failed:', errorInfo);
        }
    };

    const handleCadastrarClienteClick = () => {
        form.submit();
    };

    const handleCancelarClienteClick = () => {
        setClienteVisivel(false);
    };

    const handleCepChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const cep = e.target.value;
        if (cep.length === 8) {
            try {
                const endereco = await buscarCEP(cep);
                form.setFieldsValue({
                    state: endereco.state,
                    city: endereco.city,
                    neighborhood: endereco.neighborhood,
                    street: endereco.street,
                });
            } catch (error) {
                message.error('Erro ao buscar o CEP');
            }
        }
    };

    return (
            <div className='pt-2'>
                <ModalPadrao
                    titulo='Cadastrar Cliente'
                    textoBotao='Cadastrar'
                    onClique={handleCadastrarClienteClick}
                    onCancel={handleCancelarClienteClick}
                    visivel={clienteVisivel}
                >
                    <Form
                        form={form}
                        onFinish={handleSubmit}
                        layout="vertical"
                    >
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item
                                    name="nome"
                                    label="Nome"
                                    rules={[{ required: true, message: 'Por favor, adicione o nome do beneficiado.' }]}
                                >
                                    <Input placeholder="Nome do beneficiado" />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    name="cpf"
                                    label="CPF"
                                    rules={[()=>({validator(_, value) {
                                        return validateCPF(value)
                                          ? Promise.resolve()
                                          : Promise.reject(
                                              new Error("Por favor, adicione um CPF válido."),
                                            );
                                      },}) ]}
                                >
                                    <MaskedInput mask={"000.000.000-00"} placeholder="Informe o CPF do beneficiado" />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    name="dataNascimento"
                                    label="Data de Nascimento"
                                    rules={[{ required: true, message: 'Por favor, adicione a data de nascimento do beneficiado.' }]}
                                >
                                    <DatePicker placeholder="dd/mm/aaaa" style={{ width: '100%' }} />
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    name="telefone"
                                    label="Telefone"
                                    rules={[{ required: true, message: 'Por favor, informe um telefone para contato.' }]}
                                >
                                    <Input placeholder="(00) 0 0000-0000" />
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    name="email"
                                    label="Email"
                                    rules={[{ required: true, message: 'Por favor, informe um email para contato.' }]}
                                >
                                    <Input placeholder="Informe seu email para contato." />
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    name={["endereco","cep"]}
                                    label="CEP"
                                    rules={[{ required: true, message: 'Por favor, adicione o CEP da cidade onde reside.' }]}
                                >
                                    <Input placeholder="Informe o CEP de residência." type="number" onChange={handleCepChange} />
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    name={["endereco","logradouro"]}
                                    label="Rua"
                                    rules={[{ required: true, message: 'Por favor, informe a rua onde reside.' }]}
                                >
                                    <Input placeholder="Informe a rua de residência." />
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    name={["endereco","numero"]}
                                    label="Número"
                                    rules={[{ required: true, message: 'Por favor, informe o número de residência.' }]}
                                >
                                    <Input placeholder="Informe o número de residência." />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    name={["endereco","bairro"]}
                                    label="Bairro"
                                    rules={[{ required: true, message: 'Por favor, informe o bairro onde reside.' }]}
                                >
                                    <Input placeholder="Informe o bairro de residência." />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    name={["endereco","cidade"]}
                                    label="Cidade"
                                    rules={[{ required: true, message: 'Por favor, informe a cidade onde reside.' }]}
                                >
                                    <Input placeholder="Informe a cidade de residência." />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    name={["endereco","estado"]}
                                    label="Estado"
                                    rules={[{ required: true, message: 'Por favor, informe o estado onde reside.' }]}
                                >
                                    <Input placeholder="Informe o estado de residência." />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </ModalPadrao>
                <Button className="button w-100" onClick={() => setClienteVisivel(true)}>Novo Cliente</Button>
            </div>
    );
};

export default FormCliente;
