export const buscarCEP = async (cep: string) => {
    const response = await fetch(`https://brasilapi.com.br/api/cep/v2/${cep}`);
    if (!response.ok) {
        throw new Error('Erro ao buscar o CEP');
    }
    const data = await response.json();
    return data;
};
