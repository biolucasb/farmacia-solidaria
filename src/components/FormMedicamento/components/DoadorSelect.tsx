import { listAllPessoa } from "@infrastructure/redux/pessoa";
import { AppDispatch, RootState } from "@infrastructure/redux/store";
import { Form, Select, SelectProps } from "antd";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

const PessoaSelect: React.FC = () => {
  const { list, listLoading } = useSelector(
    (state: RootState) => state.pessoa
  );
  const dispatch = useDispatch<AppDispatch>();

  const onSearch = (value: string) => {
    dispatch(
      listAllPessoa({ pageIndex: 0, pageSize: 10, query: value })
    );
  };

  const pessoaSelectOptions: SelectProps = {
    options: list
      ? list.map((item) => ({ value: item.id, label: item.nome }))
      : [],
    showSearch: true,
    notFoundContent: "Nenhuma pessoa foi encontrada",
    loading: listLoading,
    filterOption: () => true,
    onSearch,
  };

  useEffect(() => {
    dispatch(listAllPessoa({ pageSize: 10, pageIndex: 0 }));
  }, [dispatch]);

  return (
    <Form.Item label={"Doador"} name={["doadorId"]}>
      <Select {...pessoaSelectOptions} />
    </Form.Item>
  );
};

export default PessoaSelect;
