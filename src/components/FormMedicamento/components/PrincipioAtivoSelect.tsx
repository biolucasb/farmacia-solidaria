import { listAllPrincipiosAtivos } from "@infrastructure/redux/principio-ativo";
import { AppDispatch, RootState } from "@infrastructure/redux/store";
import { Form, Select, SelectProps } from "antd";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

const PrincipioAtivoSelect: React.FC = () => {
  const form = Form.useFormInstance();
  const principioAtivoId = Form.useWatch("principioAtivoId", form);

  useEffect(() => {
    if (principioAtivoId) {
      form.setFieldValue("principioAtivo", undefined);
    }
  }, [principioAtivoId, form]);

  const { list, listLoading } = useSelector(
    (state: RootState) => state.principioAtivo
  );
  const dispatch = useDispatch<AppDispatch>();

  const onSearch = (value: string) => {
    if (value.length === 0) {
      dispatch(listAllPrincipiosAtivos({ pageIndex: 0, pageSize: 10 }));
      return;
    }
    form.setFieldValue("principioAtivo", value);
    dispatch(
      listAllPrincipiosAtivos({ pageIndex: 0, pageSize: 10, query: value })
    );
  };

  const principioativosSelectOptions: SelectProps = {
    options: list
      ? list.map((item) => ({ value: item.id, label: item.nome }))
      : [],
    showSearch: true,
    notFoundContent: "Nenhum principio ativos encontrado",
    loading: listLoading,
    filterOption: () => true,
    onSearch,
    allowClear: true,
  };


  useEffect(() => {
    dispatch(listAllPrincipiosAtivos({ pageSize: 10, pageIndex: 0 }));
  }, [dispatch]);

  return(
    <Form.Item label={"Principio Ativo"} name={["principioAtivoId"]}>
      <Select {...principioativosSelectOptions} />
    </Form.Item>
  );
};

export default PrincipioAtivoSelect;
