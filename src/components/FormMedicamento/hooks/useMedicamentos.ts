import {
  createMedicamento,
  listAllMedicamento,
  resetMedicamentoState,
} from "@infrastructure/redux/medicamento";
import { setFormOpen } from "@infrastructure/redux/pessoa";
import { AppDispatch, RootState } from "@infrastructure/redux/store";
import { Form, message } from "antd";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const useMedicamentos = () => {
  const [medicamentoVisivel, setMedicamentoVisivel] = useState(false);

  const [messageapi, contextHolders] = message.useMessage(); //manda mensagem pro

  const [form] = Form.useForm();
  const principioAtivo = Form.useWatch("principioAtivo", form);
   

  const dispatch = useDispatch<AppDispatch>();
  const { operationLoading, operationSuccess } = useSelector(
    (state: RootState) => state.medicamento
  );

  useEffect(() => {
    if (operationSuccess) {
      form.resetFields();
      setMedicamentoVisivel(false);
      messageapi.success("Medicamento Cadastrado Com Sucesso!");
      dispatch(resetMedicamentoState());
      dispatch(listAllMedicamento({ pageIndex: 0, pageSize: 10 }));
    }
  }, [operationSuccess, form, messageapi]);

  const handleCadastrarMedicamentoClick = () => {
    form
      .validateFields()
      .then((fields) => {
        dispatch(createMedicamento(fields));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleCancelarMedicamentoClick = () => {
    form.resetFields();
    setMedicamentoVisivel(false);
  };

  const openForm = () => {
    setMedicamentoVisivel(true);
  };

  const openFormPessoa = () => {
    dispatch(setFormOpen(true));
  };

  return {
    form,
    medicamentoVisivel,
    handleCadastrarMedicamentoClick,
    handleCancelarMedicamentoClick,
    openForm,
    openFormPessoa,
    principioAtivo,
    contextHolders,
    operationLoading,
  };
};

export default useMedicamentos;
