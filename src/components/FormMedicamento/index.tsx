import React, { useState } from "react";
import { Button, DatePicker, Form, Input, InputNumber, Checkbox } from "antd";
import "./style.css";
import ModalPadrao from "@components/ModalPadrao";
import { useMedicamentos } from "./hooks";
import PrincipioAtivoSelect from "./components/PrincipioAtivoSelect";
import DoadorSelect from "./components/DoadorSelect";
import dayjs from "dayjs";

const FormCadastrar: React.FC = () => {
  const {
    form,
    medicamentoVisivel,
    handleCadastrarMedicamentoClick,
    handleCancelarMedicamentoClick,
    openForm,
    openFormPessoa,
    principioAtivo,
    contextHolders,
  } = useMedicamentos();

  const [showDescription, setShowDescription] = useState(false);

  const toggleDescription = (value: boolean) => {
    setShowDescription(value);
  };

  return (
    <>
      {/* Modal para Cadastrar Medicamentos */}
      <div className="centralizar-botoes pt-2">
        {contextHolders}
        <ModalPadrao
          titulo="Cadastrar Medicamento"
          textoBotao="Cadastrar"
          onClique={handleCadastrarMedicamentoClick}
          onCancel={handleCancelarMedicamentoClick}
          visivel={medicamentoVisivel}
        >
          <Form form={form} layout="vertical">
            <Form.Item
              name="nomeComercial"
              label="Nome do Medicamento"
              rules={[{ required: true, message: "Preencha este campo." }]}
            >
              <Input autoComplete="nope" />
            </Form.Item>

            <Form.Item
              name="quantidade"
              label="Quantidade"
              rules={[{ required: true, message: "Preencha este campo." }]}
            >
              <InputNumber style={{ width: "100%" }} />
            </Form.Item>

            <PrincipioAtivoSelect />

            <Form.Item
              name="principioAtivo"
              label="Novo Principio Ativo"
              tooltip="Caso o principio ativo não esteja na lista, ele será criado. Selecione um principio ativo para limpar este campo."
              hidden={principioAtivo === undefined}
            >
              <Input autoComplete="nope" />
            </Form.Item>

            <Form.Item
              label="Concentração"
              name="concentracao"
              rules={[{ required: true, message: "Preencha este campo." }]}
            >
              <Input autoComplete="nope" />
            </Form.Item>

            <Form.Item
              label="Data de Validade"
              name="dataValidade"
              rules={[{ required: true, message: "Preencha este campo." }]}
              getValueProps={(value) => ({
                value: value && dayjs(Date.parse(value)),
              })}
              normalize={(value) =>
                value && `${dayjs(value).toDate().toISOString()}`
              }
            >
              <DatePicker />
            </Form.Item>

            <Form.Item
              name="lote"
              label="Lote"
              rules={[{ required: true, message: "Preencha este campo." }]}
            >
              <Input style={{ width: "100%" }} autoComplete="nope" />
            </Form.Item>

            <DoadorSelect />
            <Form.Item>
              <Button onClick={openFormPessoa}>Cadastrar Novo Doador</Button>
            </Form.Item>
            <Form.Item>
              <Checkbox onChange={(e) => toggleDescription(e.target.checked)}>
                Adicionar descrição
              </Checkbox>
            </Form.Item>

            {showDescription && (
              <Form.Item label="Observação" name="observacao">
                <Input.TextArea />
              </Form.Item>
            )}
          </Form>
        </ModalPadrao>
        <Button
          className="button w-100"
          style={{ width: "100%" }}
          onClick={openForm}
        >
          Novo Medicamento
        </Button>
      </div>
    </>
  );
};

export default FormCadastrar;
