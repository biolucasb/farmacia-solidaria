import { buscarCEP } from "@components/FormCliente/services/cepService";
import {
  createPessoa,
  listAllPessoa,
  resetPessoaState,
  setFormOpen,
} from "@infrastructure/redux/pessoa";
import { AppDispatch, RootState } from "@infrastructure/redux/store";
import { TipoPessoa } from "@infrastructure/types";
import { Form, message } from "antd";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const useFormPessoa = () => {
  const [form] = Form.useForm();

  const [tipoPessoa, setTipoPessoa] = useState<TipoPessoa | undefined>(
    undefined
  );

  const [messageapi, contextHolders] = message.useMessage();

  const dispatch = useDispatch<AppDispatch>();
  const { operationLoading, operationSuccess, formOpen } = useSelector(
    (state: RootState) => state.pessoa
  );

  useEffect(() => {
    if (operationSuccess) {
      form.resetFields();
      dispatch(setFormOpen(false));
      messageapi.success("Pessoa Cadastrada Com Sucesso!");
      setTipoPessoa(undefined);
      dispatch(resetPessoaState());
      dispatch(listAllPessoa({ pageIndex: 0, pageSize: 10 }));
    }
  }, [operationSuccess, form, messageapi]);

  const selectPessoa = (value: TipoPessoa) => {
    setTipoPessoa(value);
  };

  const onSubmit = () => {
    form
      .validateFields()
      .then((fields) => {
        dispatch(createPessoa(fields));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const closeForm = () => {
    form.resetFields();
    dispatch(setFormOpen(false));
    setTipoPessoa(undefined);
  };

  const openForm = () => {
    dispatch(setFormOpen(true));
  };

  const handleCepChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const cep = e.target.value;
    if (cep.length === 8) {
      try {
        const endereco = await buscarCEP(cep);
        form.setFieldValue("endereco", {
          cep,
          estado: endereco.state,
          cidade: endereco.city,
          bairro: endereco.neighborhood,
          logradouro: endereco.street,
        });
      } catch (error) {
        messageapi.error("Erro ao buscar o CEP");
      }
    }
  };

  return {
    visivel: formOpen,
    form,
    onSubmit,
    closeForm,
    openForm,
    tipoPessoa,
    selectPessoa,
    handleCepChange,
    contextHolders,
    operationLoading,
  };
};

export default useFormPessoa;