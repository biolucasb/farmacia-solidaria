import { Button, Col, Form, Input, Radio, Row } from "antd";
import useFormPessoa from "./hooks";
import ModalPadrao from "@components/ModalPadrao";
import { TipoPessoa } from "@infrastructure/types";
import { MaskedInput } from "antd-mask-input";
import { cnpjUnmask, cnpjMask } from "@utils/index";
import validateCNPJ from "@utils/validate-cnpj";
import validateCPF from "@utils/validate-cpf";

const FormPessoa: React.FC = () => {
  const {
    visivel,
    form,
    onSubmit,
    closeForm,
    openForm,
    tipoPessoa,
    selectPessoa,
    handleCepChange,
    contextHolders,
  } = useFormPessoa();

  return (
    <div className="centralizar-botoes pt-2">
      {contextHolders}
      <ModalPadrao
        titulo="Cadastrar Pessoa"
        textoBotao="Cadastrar"
        onClique={onSubmit}
        onCancel={closeForm}
        visivel={visivel}
      >
        <Form form={form} layout="vertical">
          <Row>
            <Col span={24}>
              <Form.Item
                label="Tipo"
                rules={[{ required: true, message: "Preencha este campo." }]}
              >
                <Radio.Group onChange={(e) => selectPessoa(e.target.value)}>
                  <Radio value={TipoPessoa.FISICA}>Pessoa Física</Radio>
                  <Radio value={TipoPessoa.JURIDICA}>Pessoa Jurídica</Radio>
                </Radio.Group>
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name={["nome"]}
                label="Nome"
                rules={[{ required: true, message: "Preencha este campo." }]}
              >
                <Input autoComplete="nope" />
              </Form.Item>
            </Col>
            {tipoPessoa === TipoPessoa.FISICA && (
              <Col span={24}>
                <Form.Item
                  label="CPF"
                  name="cpf"
                  rules={[
                    () => ({
                      validator(_, value) {
                        return validateCPF(value)
                          ? Promise.resolve()
                          : Promise.reject("CPF inválido.");
                      },
                    }),
                  ]}
                >
                  <MaskedInput
                    mask={"000.000.000-00"}
                    placeholder="Informe o CPF da Pessoa"
                    autoComplete="nope"
                  />
                </Form.Item>
              </Col>
            )}

            {tipoPessoa === TipoPessoa.JURIDICA && (
              <>
                <Col span={24}>
                  <Form.Item
                    label="CNPJ"
                    name={["pessoaJuridica", "cnpj", "value"]}
                    rules={[
                      () => ({
                        validator(_, value) {
                          return validateCNPJ(value)
                            ? Promise.resolve()
                            : Promise.reject("CNPJ inválido.");
                        },
                      }),
                    ]}
                    normalize={cnpjUnmask}
                    ///@ts-expect-error mismatch
                    getValueProps={cnpjMask}
                  >
                    <MaskedInput
                      mask={"00.000.000/0000-00"}
                      placeholder="Informe o CNPJ da Pessoa"
                      autoComplete="nope"
                    />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    label="Razão Social"
                    name={["pessoaJuridica", "razaoSocial"]}
                    rules={[
                      { required: true, message: "Preencha este campo." },
                    ]}
                  >
                    <Input autoComplete="nope" />
                  </Form.Item>
                </Col>
              </>
            )}
            <Col span={24}>
              <Form.Item
                name={["telefone"]}
                label="Telefone"
                rules={[{ required: true, message: "Preencha este campo." }]}
              >
                <Input autoComplete="nope" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name={["endereco", "cep"]}
                label="CEP"
                rules={[
                  {
                    required: true,
                    message: "Por favor, adicione o CEP da cidade onde reside.",
                  },
                ]}
              >
                <Input
                  placeholder="Informe o CEP de residência."
                  type="number"
                  onChange={handleCepChange}
                />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name={["endereco", "logradouro"]}
                label="Rua"
                rules={[
                  {
                    required: true,
                    message: "Por favor, informe a rua onde reside.",
                  },
                ]}
              >
                <Input placeholder="Informe a rua de residência." />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name={["endereco", "numero"]}
                label="Número"
                rules={[
                  {
                    required: true,
                    message: "Por favor, informe o número de residência.",
                  },
                ]}
              >
                <Input
                  placeholder="Informe o número de residência."
                  autoComplete="nope"
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name={["endereco", "bairro"]}
                label="Bairro"
                rules={[
                  {
                    required: true,
                    message: "Por favor, informe o bairro onde reside.",
                  },
                ]}
              >
                <Input placeholder="Informe o bairro de residência." />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name={["endereco", "cidade"]}
                label="Cidade"
                rules={[
                  {
                    required: true,
                    message: "Por favor, informe a cidade onde reside.",
                  },
                ]}
              >
                <Input placeholder="Informe a cidade de residência." />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name={["endereco", "estado"]}
                label="Estado"
                rules={[
                  {
                    required: true,
                    message: "Por favor, informe o estado onde reside.",
                  },
                ]}
              >
                <Input placeholder="Informe o estado de residência." />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </ModalPadrao>
      <Button
        className="button w-100"
        style={{ width: "100%" }}
        onClick={openForm}
      >
        Nova Pessoa
      </Button>
    </div>
  );
};

export default FormPessoa;
