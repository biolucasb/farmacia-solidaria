import { listAllMedicamento } from "@infrastructure/redux/medicamento";
import { AppDispatch, RootState } from "@infrastructure/redux/store";
import { Form, Select, SelectProps } from "antd";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

const MedicamentoSelect: React.FC = () => {

  const { list, listLoading } = useSelector(
    (state: RootState) => state.medicamento
  );
  const dispatch = useDispatch<AppDispatch>();

  const onSearch = (value: string) => {
    if (value.length === 0) {
      dispatch(listAllMedicamento({ pageIndex: 0, pageSize: 10 }));
      return;
    }
    
    dispatch(
      listAllMedicamento({ pageIndex: 0, pageSize: 10, query: value })
    );
  };

  const medicamentoSelectOptions: SelectProps = {
    options: list
      ? list.map((item) => ({ value: item.id, label: `${item.nomeComercial} - ${item.concentracao}` }))
      : [],
    showSearch: true,
    notFoundContent: "Nenhum medicamento encontrado",
    loading: listLoading,
    filterOption: () => true,
    onSearch,
    allowClear: true,
  };


  useEffect(() => {
    dispatch(listAllMedicamento({ pageSize: 10, pageIndex: 0 }));
  }, [dispatch]);

  return(
    <Form.Item label={"Medicamento"} name={["medicamentoId"]}>
      <Select {...medicamentoSelectOptions} />
    </Form.Item>
  );
};

export default MedicamentoSelect;
