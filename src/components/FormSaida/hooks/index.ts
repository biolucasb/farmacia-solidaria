import { listAllMedicamento } from "@infrastructure/redux/medicamento";
import { setFormOpen } from "@infrastructure/redux/pessoa";
import { createSaida, resetSaidaState } from "@infrastructure/redux/saida";
import { AppDispatch, RootState } from "@infrastructure/redux/store";
import { Form, message } from "antd";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const useSaidas = () => {
  const [saidaVisivel, setSaidaVisivel] = useState(false);

  const [messageapi, contextHolders] = message.useMessage(); //manda mensagem pro

  const [form] = Form.useForm();
   

  const dispatch = useDispatch<AppDispatch>();
  const { operationLoading, operationSuccess } = useSelector(
    (state: RootState) => state.saida
  );

  useEffect(() => {
    if (operationSuccess) {
      form.resetFields();
      setSaidaVisivel(false);
      messageapi.success("Saida Realizada Com Sucesso!");
      messageapi.destroy();
      dispatch(resetSaidaState());
      dispatch(listAllMedicamento({ pageIndex: 0, pageSize: 10 }));
    }
  }, [operationSuccess, form, messageapi]);

  const handleCadastrarSaidaClick = () => {
    form
      .validateFields()
      .then((fields) => {
        dispatch(createSaida(fields));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const openFormPessoa = () => {
    dispatch(setFormOpen(true));
  };

  const handleCancelarSaidaClick = () => {
    form.resetFields();
    setSaidaVisivel(false);
  };

  const openForm = () => {
    setSaidaVisivel(true);
  };

  return {
    form,
    saidaVisivel,
    handleCadastrarSaidaClick,
    handleCancelarSaidaClick,
    openForm,
    contextHolders,
    operationLoading,
    openFormPessoa,
  };
};

export default useSaidas;
