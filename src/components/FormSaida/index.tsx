import React from 'react';
import { Form, InputNumber, Row, Col, Button } from 'antd';
import './style.css';
import ModalPadrao from '@components/ModalPadrao';
import RetiranteSelect from './components/RetiranteSelect';
import MedicamentoSelect from './components/MedicamentoSelect';
import useSaidas from './hooks';

const FormSaida: React.FC = () => {


    const {
      form,
      saidaVisivel,
      handleCadastrarSaidaClick,
      handleCancelarSaidaClick,
      openForm,
      contextHolders,
      openFormPessoa,
    } = useSaidas();

    return (
      <>
        <div className="centralizar-botoes pt-2">
          {contextHolders}
          <ModalPadrao
            titulo="Saída de Medicamento"
            textoBotao="Registrar Saida"
            onClique={handleCadastrarSaidaClick}
            onCancel={handleCancelarSaidaClick}
            visivel={saidaVisivel}
          >
            <Form form={form} layout="vertical">
              <Row gutter={16}>
                <Col span={24}>
                  <RetiranteSelect />
                </Col>
                <Col span={24}>
                  <Form.Item>
                    <Button onClick={openFormPessoa}>
                      Cadastrar Novo Retirante
                    </Button>
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    label="Quantidade"
                    name="quantidade"
                    rules={[
                      {
                        required: true,
                        message: "Por favor adicione a quantidade de itens.",
                      },
                    ]}
                  >
                    <InputNumber
                      min={1}
                      placeholder="Quantidade de itens"
                      style={{ width: "100%" }}
                    />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <MedicamentoSelect />
                </Col>
              </Row>
            </Form>
          </ModalPadrao>
          <Button
            className="button w-100"
            style={{ width: "100%" }}
            onClick={openForm}
          >
            Nova Saida
          </Button>
        </div>
      </>
    );
};

export default FormSaida;
