import React from "react";
import { Layout, Menu } from "antd";
import { Link } from "react-router-dom";
import { Image } from "antd";
import Logo from "../../assets/logo.png";
import "./style.css";
import { useAuth } from "@services/auth";

const { Header } = Layout;

const ComponentHeader: React.FC = () => {
  const { isAdmin } = useAuth();
  return (
    <Header className="bg-header">
      <div className="header-menu-container">
        <Menu
          theme="light"
          mode="horizontal"
          defaultSelectedKeys={["1"]}
          className="header-menu"
        >
          <Menu.Item key="6">
            <Link className="fs-5" to="/">
              <Image width={200} src={Logo} preview={false} />
            </Link>
          </Menu.Item>
          <Menu.Item key="1">
            <Link className="fs-5" to="/">
              Tela Inicial
            </Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link className="fs-5" to="/Relatorio">
              Relatórios
            </Link>
          </Menu.Item>
          <Menu.Item key="3">
            <Link className="fs-5 txt" to="/Clientes">
              Clientes
            </Link>
          </Menu.Item>
          {isAdmin && (
            <Menu.Item key="4">
              <Link className="fs-5 txt" to="/Clientes">
                Funcionários
              </Link>
            </Menu.Item>
          )}
          <Menu.Item key="5">
            <Link className="fs-5 txt" to="/logout">
              Sair                
            </Link>
          </Menu.Item>
        </Menu>
      </div>
    </Header>
  );
};

export default ComponentHeader;
