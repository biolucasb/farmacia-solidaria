import React from 'react';
import FormCadastrar from "@components/FormMedicamento";
import FormSaida from "@components/FormSaida";
import FormPessoa from "@components/FormPessoa";
import { List, Card } from 'antd';
import './style.css';

const ComponentMenu: React.FC = () => {
    return (
      <Card title="MENU RÁPIDO" bordered={false}>
        <List
          itemLayout="vertical"
          style={{ width: "100%" }}
          dataSource={[
            { component: <FormPessoa /> },
            { component: <FormCadastrar /> },
            { component: <FormSaida /> },
          ]}
          renderItem={(item) => (
            <List.Item style={{ width: "100%" }}>{item.component}</List.Item>
          )}
        />
      </Card>
    );
};

export default ComponentMenu;
