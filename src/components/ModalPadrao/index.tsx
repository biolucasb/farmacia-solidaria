import React from 'react';
import { Modal, Button } from 'antd';
import { ModalPadraoProps } from '@infrastructure/types';

const ModalPadrao: React.FC<ModalPadraoProps> = ({
  titulo,
  textoBotao,
  textoCancelar = 'Cancelar',
  onClique,
  onCancel,
  visivel,
  children,
}) => {
  return (
    <Modal
      title={titulo}
      open={visivel}
      onCancel={onCancel}
      maskClosable={false}
      footer={[
        <Button key="cancel" onClick={onCancel}>
          {textoCancelar}
        </Button>,
        <Button key="submit" type="primary" onClick={onClique}>
          {textoBotao}
        </Button>,
      ]}
    >
      {children}
    </Modal>
  );
};

export default ModalPadrao;
