import { listAllMedicamento } from "@infrastructure/redux/medicamento";
import { setFormOpen } from "@infrastructure/redux/pessoa";
import { createSaida } from "@infrastructure/redux/saida";
import { AppDispatch, RootState } from "@infrastructure/redux/store";
import { Form, message } from "antd";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

const useSaidas = (closeCallback: () => void) => {
  const [messageapi, contextHolders] = message.useMessage(); //manda mensagem pro

  const [form] = Form.useForm();
   

  const dispatch = useDispatch<AppDispatch>();
  const { operationLoading, operationSuccess } = useSelector(
    (state: RootState) => state.saida
  );

  useEffect(() => {
    if (operationSuccess) {
      form.resetFields();
      closeCallback();
      messageapi.success("Saida Realizada Com Sucesso!");
      dispatch(listAllMedicamento({ pageIndex: 0, pageSize: 10 }));
    }
  }, [operationSuccess, form, messageapi]);

  const handleCadastrarSaidaClick = () => {
    form
      .validateFields()
      .then((fields) => {
        dispatch(createSaida(fields));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleCancelarSaidaClick = () => {
    form.resetFields();
    closeCallback();
  };

  const openFormPessoa = () => {
    dispatch(setFormOpen(true));
  };
  return {
    form,
    handleCadastrarSaidaClick,
    handleCancelarSaidaClick,
    openFormPessoa,
    contextHolders,
    operationLoading,
  };
};

export default useSaidas;
