import React from 'react';
import { FormSaidaAutomaticaProps } from '@infrastructure/types';
import { Form, InputNumber, Row, Col, Input, Button } from "antd";
import "./style.css";
import ModalPadrao from "@components/ModalPadrao";
import RetiranteSelect from "./components/RetiranteSelect";
import useSaidas from "./hooks";

const FormSaidaAutomatica: React.FC<FormSaidaAutomaticaProps> = ({
  medicamento,
  visivel,
  closeCallback,
}) => {
  const {
    form,
    handleCadastrarSaidaClick,
    handleCancelarSaidaClick,
    contextHolders,
    openFormPessoa,
  } = useSaidas(closeCallback);

  return (
    <>
      <div className="centralizar-botoes pt-2">
        {contextHolders}
        <ModalPadrao
          titulo="Saída de Medicamento"
          textoBotao="Registrar Saida"
          onClique={handleCadastrarSaidaClick}
          onCancel={handleCancelarSaidaClick}
          visivel={visivel}
        >
          <Form form={form} layout="vertical">
            <Row gutter={16}>
              <Col span={24}>
                <RetiranteSelect />
              </Col>
              <Col span={24}>
                <Form.Item>
                  <Button onClick={openFormPessoa}>
                    Cadastrar Novo Retirante
                  </Button>
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item
                  label="Quantidade"
                  name="quantidade"
                  rules={[
                    {
                      required: true,
                      message: "Por favor adicione a quantidade de itens.",
                    },
                  ]}
                >
                  <InputNumber
                    min={1}
                    placeholder="Quantidade de itens"
                    style={{ width: "100%" }}
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <Input
                  readOnly
                  value={`${medicamento.nomeComercial} - ${medicamento.concentracao} - ${medicamento.lote}`}
                />
              </Col>
            </Row>
            <Form.Item
              name="medicamentoId"
              hidden={true}
              initialValue={medicamento.id}
            ></Form.Item>
          </Form>
        </ModalPadrao>
      </div>
    </>
  );
};

export default FormSaidaAutomatica;
