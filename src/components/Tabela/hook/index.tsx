import { listAllMedicamento } from "@infrastructure/redux/medicamento";
import { AppDispatch, RootState } from "@infrastructure/redux/store";
import { Medicamento, ToastType } from "@infrastructure/types";
import { showToast } from "@utils/toast";
import { Badge, Button } from "antd";
import { useEffect, useMemo, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

const useMedicamento = () => {
  const [medicamento, setMedicamento] = useState<Medicamento | undefined>(
    undefined
  );
  const [visivel, setVisivel] = useState(false);
  const [isDescarte, setIsDescarte] = useState(false);

  const closeCallback = () => {
    setVisivel(false);
    setMedicamento(undefined);
  };
  const iniciarSaida = (medicamento: Medicamento) => {
    if (isDescarte) {
      showToast(
        "Medicamento vencido, não é possível registrar saída",
        ToastType.ERROR
      );
      return;
    }
    setMedicamento(medicamento);
    setVisivel(true);
  };

  const { list, listLoading } = useSelector(
    (state: RootState) => state.medicamento
  );
  const dispatch = useDispatch<AppDispatch>();

  const columns = useMemo(
    () => [
      {
        title: "Nome Comercial",
        dataIndex: "nomeComercial",
        key: "nome",
      },

      {
        title: "Data de Validade",
        dataIndex: "dataValidade",
        key: "dataValidade",
      },
      {
        title: "Lote",
        dataIndex: "lote",
        key: "lote",
      },
      {
        title: "Principio Ativo",
        dataIndex: "principioAtivo",
        key: "principioAtivo",
      },
      {
        title: "Concentração",
        dataIndex: "concentracao",
        key: "concentracao",
      },
      {
        title: "Quantidade",
        dataIndex: "quantidade",
        key: "quantidade",
      },
      {
        title: "Registrar Saída",
        key: "registrarSaida",
        render: (_: never, record: Medicamento) =>
          isDescarte ? (
            <Badge.Ribbon text="Medicamento Vencido" color="red">
              <div>oi</div>
            </Badge.Ribbon>
          ) : (
            <Button
              onClick={() => {
                iniciarSaida(record);
              }}
              disabled={isDescarte}
              type="primary"
            >
              Registrar Saída
            </Button>
          ),
      },
    ],
    [isDescarte]
  );

  const onSearch = (value: string) => {
    setIsDescarte(value.toLowerCase() === "descarte");
    if (value.length === 0) {
      dispatch(listAllMedicamento({ pageIndex: 0, pageSize: 10 }));
      return;
    }

    dispatch(listAllMedicamento({ pageIndex: 0, pageSize: 10, query: value }));
  };

  useEffect(() => {
    dispatch(listAllMedicamento({ pageSize: 10, pageIndex: 0 }));
  }, [dispatch]);

  const dataSource = useMemo(() => {
    return list.map((x) => ({
      key: x.id,
      ...x,
      dataValidade: new Date(x.dataValidade).toLocaleDateString() || "",
    }));
  }, [list]);

  return {
    list,
    listLoading,
    columns,
    onSearch,
    dataSource,
    closeCallback,
    visivel,
    medicamento,
    isDescarte,
  };
};

export default useMedicamento;
