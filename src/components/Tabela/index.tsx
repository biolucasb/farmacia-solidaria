import React from "react";
import { Input, Table } from "antd";
import useMedicamento from "./hook";
import FormSaidaAutomatica from "./FormSaida";
import { FaSearch } from "react-icons/fa";

const { Search } = Input;
const ComponentTabela: React.FC = () => {
  const {
    columns,
    dataSource,
    closeCallback,
    visivel,
    medicamento,
    listLoading,
    onSearch,
  } = useMedicamento();
  return (
    <>
      <Search
        onChange={(e) => onSearch(e.target.value)}
        placeholder="Buscar"
        enterButton={<FaSearch />}
        size="large"
        style={{ width: "100%", marginBottom: "20px" }}
      />
      <Table
        //@ts-expect-error - dataSource is not assignable to type 'any[]'
        columns={columns}
        //@ts-expect-error - dataSource is not assignable to type 'any[]'
        dataSource={dataSource}
        loading={listLoading}
        size="small"
        bordered
        pagination={false}
      />
      {visivel && medicamento && (
        <FormSaidaAutomatica
          medicamento={medicamento}
          closeCallback={closeCallback}
          visivel={visivel}
        />
      )}
    </>
  );
};

export default ComponentTabela;
