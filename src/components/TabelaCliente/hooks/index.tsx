import { listAllPessoa } from "@infrastructure/redux/pessoa";
import { AppDispatch, RootState } from "@infrastructure/redux/store";
import { TableProps } from "antd";
import { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";

const usePessoa = () => {

  const { list, listLoading } = useSelector((state: RootState) => state.pessoa);
  const dispatch = useDispatch<AppDispatch>();

  const onSearch = (value: string) => {
    dispatch(listAllPessoa({ pageIndex: 0, pageSize: 10, query: value }));
  };

  useEffect(() => {
    dispatch(listAllPessoa({ pageSize: 10, pageIndex: 0 }));
  }, [dispatch]);

  const columns:TableProps['columns'] = [
    {
        key: 'nome',
        title: 'Nome',
        dataIndex: 'nome',
    },
    {
        key: 'cpf',
        title: 'CPF',
        dataIndex: 'cpf',
    },
    {
        key: 'telefone',
        title: 'Telefone',
        dataIndex: 'telefone',
    },
    {
        key: 'bairro',
        title: 'Bairro',
        dataIndex: 'bairro',
    }
  ];

    const dataSource = useMemo(() => {
        return list.map(x => ({
          key: x.id,
          bairro: x.endereco?.bairro,
          ...x,
        }))
      }, [list])

  return { columns, listLoading, onSearch, dataSource };

};

export default usePessoa;