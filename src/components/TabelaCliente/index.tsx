import React from 'react';
import { Flex, Input, Table } from 'antd';
import usePessoa from './hooks';
import { FaSearch } from 'react-icons/fa';
import Loading from "@components/Loading";

const { Search } = Input;
const TabelaCliente: React.FC = () => {
  const { columns, listLoading, onSearch, dataSource } = usePessoa();

  return (
    <Flex vertical justify="center" align="center" gap={25}>
      <Search
        placeholder="Buscar"
        enterButton={<FaSearch />}
        onChange={(e) => onSearch(e.target.value)}
        size="large"
        className="mb-3"
      />
      {listLoading && <Loading />}
      <Table
        columns={columns}
        dataSource={dataSource}
        size="small"
        bordered
        pagination={false}
        style={{ width: "100%" }}
      />
    </Flex>
  );
};

export default TabelaCliente;
