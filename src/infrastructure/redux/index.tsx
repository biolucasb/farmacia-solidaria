export { default as principioAtivoReducer } from "./principio-ativo";
export { default as pessoaReducer } from "./pessoa";
export { default as medicamentoReducer } from "./medicamento";
export { default as saidaReducer } from "./saida";
export { store } from "./store";

