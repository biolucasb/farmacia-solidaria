import { createSlice } from "@reduxjs/toolkit";
import {
  createMedicamento,
  listAllMedicamento,
  resetMedicamentoState,
} from "./reducers";
import { PaginatedResponse } from "@infrastructure/types/default.types";
import { Medicamento, MedicamentoState } from "@infrastructure/types";
import { OperationType } from "@infrastructure/types/base.enums";

const initialState: MedicamentoState = {
  entityLoading: false,
  entity: undefined,
  listLoading: false,
  list: [],
  listData: {} as PaginatedResponse<Medicamento>,
  operationType: OperationType.CRIAR,
  operationLoading: false,
  operationSuccess: false,
  entityToOperate: undefined,
};

const redux = createSlice({
  name: "medicamento",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(createMedicamento.pending, (state) => {
      state.operationLoading = true;
    });
    builder.addCase(createMedicamento.fulfilled, (state) => {
      state.operationLoading = false;
      state.operationSuccess = true;
    });
    builder.addCase(createMedicamento.rejected, (state) => {
      state.operationLoading = false;
      state.operationSuccess = false;
    });
    builder.addCase(listAllMedicamento.pending, (state) => {
      state.listLoading = true;
    });
    builder.addCase(listAllMedicamento.fulfilled, (state, action) => {
      state.listLoading = false;
      state.list = action.payload.data;
      state.listData = action.payload;
      console.log(action.payload);
    });
    builder.addCase(listAllMedicamento.rejected, (state) => {
      state.listLoading = false;
    });
    builder.addCase(resetMedicamentoState, () => initialState);
  },
});

export default redux.reducer;

export { createMedicamento, listAllMedicamento, resetMedicamentoState };
