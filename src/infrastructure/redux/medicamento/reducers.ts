import { CreateMedicamento, ListMedicamentoResponseType } from "@infrastructure/types";
import { PaginationParameters } from "@infrastructure/types/default.types";
import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import { AxiosApi as axios } from "@services/axios";

export const createMedicamento = createAsyncThunk(
  "medicamento/createMedicamento",
  async (req: CreateMedicamento) => {
    const { data } = await axios.post(`/medicamentos`, req);
    return data;
  }
);

export const listAllMedicamento = createAsyncThunk(
  "medicamento/listAllMedicamento",
  async (pagination: PaginationParameters) => {
    const { pageSize, pageIndex } = pagination;
    const { data } = await axios.get<ListMedicamentoResponseType>(
      `/medicamentos/${pageSize}/${pageIndex}`,
      {
        params: {
          query: pagination.query,
        },
      }
    );

    return data;
  }
);

export const resetMedicamentoState = createAction(
  "medicamento/resetMedicamentoState"
);