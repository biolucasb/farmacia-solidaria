import { createSlice } from "@reduxjs/toolkit";
import {
  createPessoa,
  listAllPessoa,
  resetPessoaState,
  setFormOpen,
} from "./reducers";
import { PaginatedResponse } from "@infrastructure/types/default.types";
import { Pessoa, PessoaState } from "@infrastructure/types";
import { OperationType } from "@infrastructure/types/base.enums";

const initialState: PessoaState = {
  formOpen: false,
  entityLoading: false,
  entity: undefined,
  listLoading: false,
  list: [],
  listData: {} as PaginatedResponse<Pessoa>,
  operationType: OperationType.CRIAR,
  operationLoading: false,
  operationSuccess: false,
  entityToOperate: undefined,
};

const redux = createSlice({
  name: "pessoa",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(createPessoa.pending, (state) => {
      state.operationLoading = true;
    });
    builder.addCase(createPessoa.fulfilled, (state) => {
      state.operationLoading = false;
      state.operationSuccess = true;
    });
    builder.addCase(createPessoa.rejected, (state) => {
      state.operationLoading = false;
      state.operationSuccess = false;
    });
    builder.addCase(listAllPessoa.pending, (state) => {
      state.listLoading = true;
    });
    builder.addCase(listAllPessoa.fulfilled, (state, action) => {
      state.listLoading = false;
      state.list = action.payload.data;
      state.listData = action.payload;
      console.log(action.payload);
    });
    builder.addCase(listAllPessoa.rejected, (state) => {
      state.listLoading = false;
    });
    builder.addCase(setFormOpen, (state, action) => {
      state.formOpen = action.payload;
    });
    builder.addCase(resetPessoaState, () => initialState);
  },
});

export default redux.reducer;

export { createPessoa, listAllPessoa, setFormOpen, resetPessoaState };
