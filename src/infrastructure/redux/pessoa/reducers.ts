import { CreatePessoa } from "@infrastructure/types";
import { PaginationParameters } from "@infrastructure/types/default.types";
import { ListPessoaResponseType } from "@infrastructure/types/pessoa.type";
import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import { AxiosApi as axios } from "@services/axios";

export const createPessoa = createAsyncThunk(
  "pessoa/createPessoa",
  async (req: CreatePessoa) => {
    const { data } = await axios.post(`/pessoa`, req);
    return data;
  }
);

export const listAllPessoa = createAsyncThunk(
  "pessoa/listAllPessoa",
  async (pagination: PaginationParameters) => {
    const { pageSize, pageIndex } = pagination;
    const { data } = await axios.get<ListPessoaResponseType>(
      `/pessoa/${pageSize}/${pageIndex}`,
      {
        params: {
          query: pagination.query,
        },
      }
    );

    return data;
  }
);
export const setFormOpen = createAction<boolean>("pessoa/setFormOpen");

export const resetPessoaState = createAction("pessoa/resetPessoaState");
