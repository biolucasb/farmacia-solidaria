import {
  PrincipioAtivoResponseType,
  PrincipioAtivoState,
} from "@infrastructure/types/principio-ativo.types";
import { createSlice } from "@reduxjs/toolkit";
import { listAllPrincipiosAtivos } from "./reducers";
import { PaginatedResponse } from "@infrastructure/types/default.types";

const initialState: PrincipioAtivoState = {
  entityLoading: false,
  entity: undefined,
  listLoading: false,
  list: [],
  listData: {} as PaginatedResponse<PrincipioAtivoResponseType>,
  operationType: 0,
  operationLoading: false,
  operationSuccess: false,
  entityToOperate: undefined,
};

const redux = createSlice({
  name: "principio-ativo",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(listAllPrincipiosAtivos.pending, (state) => {
      state.listLoading = true;
    });
    builder.addCase(listAllPrincipiosAtivos.fulfilled, (state, action) => {
      state.listLoading = false;
      state.list = action.payload.data;
    });
    builder.addCase(listAllPrincipiosAtivos.rejected, (state) => {
      state.listLoading = false;
    });
  },
});

export default redux.reducer;

export { listAllPrincipiosAtivos };
