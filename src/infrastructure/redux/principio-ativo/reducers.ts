import { PaginationParameters } from "@infrastructure/types/default.types";
import { ListPrincipioAtivoResponseType } from "@infrastructure/types/principio-ativo.types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { AxiosApi as axios } from "@services/axios";

export const listAllPrincipiosAtivos = createAsyncThunk(
  "principio-ativo/listAllPrincipiosAtivos",
  async (pagination: PaginationParameters) => {
    const { pageSize, pageIndex } = pagination;
    const { data } = await axios.get<ListPrincipioAtivoResponseType>(
      `/principioativo/${pageSize}/${pageIndex}`,
      {
        params: {
          query: pagination.query,
        },
      }
    );

    return data;
  }
);
