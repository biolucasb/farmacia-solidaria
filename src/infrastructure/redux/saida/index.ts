import {  SaidaState } from "@infrastructure/types";
import { createSaida, listAllSaida, resetSaidaState } from "./reducer";
import { PaginatedResponse } from "@infrastructure/types/default.types";
import { createSlice } from "@reduxjs/toolkit";
import { OperationType } from "@infrastructure/types/base.enums";
import { MovimentacaoResponse } from "@infrastructure/types/saida.type";

const initialState: SaidaState = {
  entityLoading: false,
  entity: undefined,
  listLoading: false,
  list: [],
  listData: {} as PaginatedResponse<MovimentacaoResponse>,
  operationType: OperationType.CRIAR,
  operationLoading: false,
  operationSuccess: false,
  entityToOperate: undefined,
};

const redux = createSlice({
  name: "pessoa",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(createSaida.pending, (state) => {
      state.operationLoading = true;
    });
    builder.addCase(createSaida.fulfilled, (state) => {
      state.operationLoading = false;
      state.operationSuccess = true;
    });
    builder.addCase(createSaida.rejected, (state) => {
      state.operationLoading = false;
      state.operationSuccess = false;
    });
    builder.addCase(listAllSaida.pending, (state) => {
      state.listLoading = true;
    });
    builder.addCase(listAllSaida.fulfilled, (state, action) => {
      state.listLoading = false;
      state.list = action.payload.data;
      state.listData = action.payload;
      console.log(action.payload);
    });
    builder.addCase(listAllSaida.rejected, (state) => {
      state.listLoading = false;
    });
    builder.addCase(resetSaidaState, () => {
      return initialState;
    });
  },
});

export default redux.reducer;

export { createSaida, listAllSaida, resetSaidaState };