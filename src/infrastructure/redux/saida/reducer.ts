import { CreateSaida, ListSaidaResponseType } from "@infrastructure/types";
import { SaidaPaginationParameters } from "@infrastructure/types/default.types";
import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import { AxiosApi as axios } from "@services/axios";

export const createSaida = createAsyncThunk(
  "saida/createSaida",
  async (req: CreateSaida) => {
    const { data } = await axios.post(`movimentacoes/saida`, req);
    return data;
  }
);

export const listAllSaida = createAsyncThunk(
  "saida/listAllSaida",
  async (pagination: SaidaPaginationParameters) => {
    const { pageSize, pageIndex } = pagination;
    const { data } = await axios.get<ListSaidaResponseType>(
      `/movimentacoes/${pageSize}/${pageIndex}`,
      {
        params: {
          query: pagination.query,
          tipo: pagination.tipo,
        },
      }
    );

    return data;
  }
);

export const resetSaidaState = createAction("saida/resetSaidaState");