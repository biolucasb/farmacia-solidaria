import { configureStore } from "@reduxjs/toolkit";
import {
  medicamentoReducer,
  pessoaReducer,
  principioAtivoReducer,
  saidaReducer,
} from ".";

export const store = configureStore({
  reducer: {
    principioAtivo: principioAtivoReducer,
    pessoa: pessoaReducer,
    medicamento: medicamentoReducer,
    saida: saidaReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
