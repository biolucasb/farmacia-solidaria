import { Outlet, useNavigate } from "react-router-dom";
import Header from "@components/Header";
import { useAuth } from "@services/auth";
import { useEffect } from "react";

const PrivateLayout = () => {
  const navigate = useNavigate();
  const { logged, loading } = useAuth();
  useEffect(() => {
    if (!loading && !logged) {
      navigate("/login");
    }
  }, [loading, logged, navigate]);
  return (
    <>
      <Header></Header>
      <Outlet></Outlet>
    </>
  );
};

export default PrivateLayout;
