import { Route, Routes } from "react-router-dom";
import {
  CadastroComponent,
  LoginComponent,
  LogoutComponent,
  RecuperarSenhaComponent,
} from "@pages/Public";
import { HomeComponent, PerfilComponent } from "@pages/Private";
import PrivateLayout from "./PrivateLayout";
import ClientesRetiradas from "@pages/Private/Clientes";

import RegistroUser from "@pages/Private/Cadastrar";
import RelatoriosPage from "@pages/Private/Relatorios";

const AppRoutes: React.FC = () => {
  return (
    <Routes>
      <Route path="/login" element={<LoginComponent />} />
      <Route path="/logout" element={<LogoutComponent />} />
      <Route path="/recuperar-senha" element={<RecuperarSenhaComponent />} />
      <Route path="/cadastro" element={<CadastroComponent />} />
      <Route path="/registro-user" element={<RegistroUser />} />
      {/*Private Routes*/}
      <Route element={<PrivateLayout />}>
        <Route path="/clientes" element={<ClientesRetiradas />} />
        <Route path="/" element={<HomeComponent />} />
        <Route path="/perfil" element={<PerfilComponent />} />
        <Route path="/relatorio" element={<RelatoriosPage />} />
      </Route>
    </Routes>
  );
};

export default AppRoutes;
