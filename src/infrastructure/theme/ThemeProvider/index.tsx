import { ThemeProviderProps } from "@infrastructure/types";
import { ConfigProvider } from "antd";
import ptBR from "antd/lib/locale/pt_BR";
const ThemeProvider: React.FC<ThemeProviderProps> = ({ children }) => {
  return <ConfigProvider locale={ptBR}>{children}</ConfigProvider>;
};

export default ThemeProvider;
