import { User } from "firebase/auth";

export type AuthContextType = {
  user: User | null | undefined;
  loading: boolean;
  access_token: string | null;
  logged: boolean;
  isAdmin: boolean;
  login: (email: string, password: string) => Promise<void>;
  logout: () => void;
};
