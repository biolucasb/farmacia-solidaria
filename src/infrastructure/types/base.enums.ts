export enum TipoPessoa {
    FISICA = 0,
    JURIDICA = 1,
}

export enum OperationType{
    CRIAR = 0,
    EDITAR = 1,
}

export enum ToastType {
  INFO = "info",
  SUCCESS = "success",
  WARNING = "warning",
  ERROR = "error",
  DEFAULT = "default",
}

export enum TipoMovimentacao {
  SAIDA,
  DOACAO,
  DESCARTE,
  AMBOS,
}