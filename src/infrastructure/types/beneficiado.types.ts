export type Beneficiado = {
  nome: string;
  cpf: string;
  dataNascimento: string;
};
