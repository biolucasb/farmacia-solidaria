export type ClientMedication = {
    nome: string;
    dataNacimento: string;
    cidade: string;
    medicamento: string;
    retirante: string;
    dataRetirada: string;
}