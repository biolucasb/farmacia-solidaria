import { ReactElement } from "react";
import { Medicamento } from "./medicamento.type";

type ComponentWithChildren = {
  children: ReactElement;
};

//Props de componentes
export type ModalPadraoProps = {
  titulo: string;
  textoBotao: string;
  textoCancelar?: string;
  onClique?: () => void;
  onCancel?: () => void;
  visivel: boolean;
  children: ReactElement;
};

export type FormSaidaAutomaticaProps = {
  medicamento: Medicamento;
  visivel: boolean;
  closeCallback: () => void;
  }

export type ThemeProviderProps = ComponentWithChildren;

export type AuthContextProviderProps = ComponentWithChildren;
