import { TipoMovimentacao } from "./base.enums";

export type PaginationParameters = {
  pageSize: number;
  pageIndex: number;
  query?: string | undefined;
};

export type SaidaPaginationParameters = PaginationParameters & {
  tipo?: TipoMovimentacao | undefined;
};

export type PaginatedResponse<T> = {
  data: T[];
  totalResult: number;
  pageSize: number;
  pageIndex: number;
  query?: string;
};
export type PatternError = {
  code: string;
  message: string;
};
export type PatternValidationError<T> = {
  title: string;
  detail: string;
  errors: T;
};
