import { PaginatedResponse } from "./default.types";
import { BaseReduxState } from "./redux.types";

export type Medicamento = {
    id: string;
    nomeComercialId?: string;
    nomeComercial?: string;
    principioAtivoId?: string;
    principioAtivo?: string;
    doadorId: string;
    dataValidade: Date;
    concentracao: string;
    quantidade: number;
    lote: string;
    observacao?: string;
  }

export type CreateMedicamento = Omit<Medicamento,"id">

export type MedicamentoState = BaseReduxState<Medicamento>;
export type ListMedicamentoResponseType = PaginatedResponse<Medicamento>
