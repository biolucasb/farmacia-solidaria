import { PaginatedResponse } from "./default.types";
import { Endereco } from "./endereco.type";
import { BaseReduxState } from "./redux.types";

export type Pessoa = {
    id: string;
    nome: string;
    cpf?: string;
    telefone?:string;
    pessoaJuridica?: PessoaJuridica;
    endereco?: Endereco;
    observacao?: string;
}

export type PessoaJuridica = {
    razaoSocial: string;
    cnpj: string;
}

export type CreatePessoa = Omit<Pessoa,"id">

export type PessoaState = BaseReduxState<Pessoa> & {
  formOpen: boolean;
};
export type ListPessoaResponseType = PaginatedResponse<Pessoa>
