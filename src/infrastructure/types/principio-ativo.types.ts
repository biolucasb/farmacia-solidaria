import { PaginatedResponse } from "./default.types";
import { BaseReduxState } from "./redux.types";

export type PrincipioAtivoResponseType = {
  id: string;
  nome: string;
};

export type ListPrincipioAtivoResponseType =
  PaginatedResponse<PrincipioAtivoResponseType>;

export type PrincipioAtivoState = BaseReduxState<PrincipioAtivoResponseType>;
