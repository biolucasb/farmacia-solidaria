import { OperationType } from "./base.enums";
import { PaginatedResponse } from "./default.types";

export type EntityState<T> = {
  entityLoading: boolean;
  entity: T | undefined;
};

export type ListState<T> = {
  listLoading: boolean;
  list: T[];
  listData: PaginatedResponse<T>;
};

export type OperationState<T> = {
  operationType: OperationType;
  operationLoading: boolean;
  operationSuccess: boolean;
  entityToOperate?: T | undefined;
};

export type BaseReduxState<T> = EntityState<T> & ListState<T> & OperationState<T>;
export type BaseReduxStateWithCustomResponse<T, U> = EntityState<T> &
  ListState<T> &
  OperationState<U>;