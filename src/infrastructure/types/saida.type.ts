import { TipoMovimentacao } from "./base.enums";
import { PaginatedResponse } from "./default.types";
import { Medicamento } from "./medicamento.type";
import { Pessoa } from "./pessoa.type";
import { BaseReduxStateWithCustomResponse } from "./redux.types";

export type Saida = {
  nome: string;
  cpf?: string;
  nomeComercialId?: string;
  nomeComercial?: string;
  principioAtivoId?: string;
  principioAtivo?: string;
  dataRetirada: Date;
};

export type MovimentacaoResponse = {
  id: string;
  retirante: Pessoa;
  dataRetirada: Date;
  medicamento: Medicamento;
  observacao?: string;
  tipo: TipoMovimentacao;
  quantidade: number;
};

export type CreateSaida = Omit<Saida, "id">;

export type SaidaState = BaseReduxStateWithCustomResponse<
  MovimentacaoResponse,
  Saida
>;
export type ListSaidaResponseType = PaginatedResponse<MovimentacaoResponse>;
