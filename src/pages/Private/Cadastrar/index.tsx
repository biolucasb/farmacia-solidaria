import React, { useState } from 'react';
import { Button, Input, Select, Form } from 'antd';
import { Link } from 'react-router-dom';
import './style.css';

const { Option } = Select;

const RegistroUser: React.FC = () => {
  const [selectedRole, setSelectedRole] = useState<string>('');
  const [adminPassword, setAdminPassword] = useState<string>('');
  const [error, setError] = useState<string>('');

  const handleRoleChange = (value: string) => {
    setSelectedRole(value);
  };

  const handlePasswordChange = (value: string) => {
    setAdminPassword(value);
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (adminPassword !== 'adm123') {
      setError('Senha incorreta');
    } else {
      setError('');
      console.log('Cadastro realizado com sucesso');
    }
  };

  return (
    <div className="registration-container">
      <div className="registration-form">
        <h3 className="registration-title">Cadastro de usuários</h3>
        {/*//@ts-expect-error FormType Mismatch */}
        <Form onSubmit={handleSubmit}>
          <Form.Item label="Nome" className="form-item">
            <Input type="text" placeholder="Nome" />
          </Form.Item>

          <Form.Item label="Email" className="form-item">
            <Input type="email" placeholder="Email" />
          </Form.Item>

          <Form.Item label="Senha" className="form-item">
            <Input.Password
              placeholder="Senha"
              onChange={(e) => handlePasswordChange(e.target.value)}
            />
          </Form.Item>

          <Form.Item label="Confirme a Senha" className="form-item">
            <Input.Password placeholder="Confirme a Senha" />
          </Form.Item>

          <Form.Item label="Senha do Administrador" className="form-item">
            <Input.Password
              placeholder="Senha"
              value={adminPassword}
              onChange={(e) => handlePasswordChange(e.target.value)}
            />
            {error && <span className="error-message">{error}</span>}
          </Form.Item>

          <Form.Item label="Função" className="form-item">
            <Select value={selectedRole} onChange={handleRoleChange}>
              <Option value="">Selecione uma Função</Option>
              <Option value="Admin">Admin</Option>
              <Option value="User">Funcionário</Option>
            </Select>
          </Form.Item>

          <Button
            className="registration-button"
            type="primary"
            htmlType="submit"
          >
            Cadastrar
          </Button>
        </Form>

        <Link to="/">
          <Button className="back-button" type="default">
            Voltar à página inicial
          </Button>
        </Link>
      </div>
    </div>
  );
};

export default RegistroUser;
