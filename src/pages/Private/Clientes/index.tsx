import React from 'react';
import './style.css';
import TabelaCliente from '@components/TabelaCliente';

const ClientesRetiradas: React.FC = () => {

  return (
      <div className="center">
        <div style={{width: "80%", }}>
          <div>
            <h1>Clientes</h1>
          </div>
          <TabelaCliente />
        </div>
      </div>
  );
};

export default ClientesRetiradas;
