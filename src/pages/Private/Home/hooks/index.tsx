import { listAllMedicamento } from "@infrastructure/redux/medicamento";
import { AppDispatch } from "@infrastructure/redux/store";
import { useDispatch } from "react-redux";

const useHomePage = () => {

    const dispatch = useDispatch<AppDispatch>();
    const onSearch = (value: string) => {
        dispatch(
            listAllMedicamento({ pageIndex: 0, pageSize: 10, query: value })
        );
    };
    return {
        onSearch
    }

}

export default useHomePage;