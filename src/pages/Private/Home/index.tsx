// HomeComponent.tsx
import React from 'react';
import { Layout, Row, Col } from "antd";
import Tabela from "../../../components/Tabela";
import Menu from "../../../components/Menu";
import "./style.css";

const { Content } = Layout;
// comentario aleatorio
const HomeComponent: React.FC = () => {
  return (
    <Layout className="pt-3 container">
      <Content className="custom-content">
        {" "}
        {/* Aplica a classe ao Content */}
        <Row gutter={[16, 16]}>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <h1 className="txt-center">Medicamentos</h1>
          </Col>
          <Row style={{ width: "100%" }} justify={"space-between"}>
            <Col xs={24} sm={24} md={24} lg={24} xl={6}>
              <Menu />
            </Col>
            <Col xs={24} sm={24} md={24} lg={24} xl={17}>
              <Tabela />
            </Col>
          </Row>
        </Row>
      </Content>
    </Layout>
  );
};

export default HomeComponent;
