import { Typography } from 'antd';

const { Title, Paragraph } = Typography;

const PerfilComponent: React.FC = () => {
  return (
    <>
      <div
        className="w-100 d-flex flex-column"
        style={{ backgroundColor: "#FF0000", padding: '20px' }}
      >
        Pera
      </div>
      <div style={{ padding: '20px' }}>
        <Typography>
          <Title level={2}>Teste</Title>
          <Paragraph>Teste</Paragraph>
        </Typography>
      </div>
    </>
  );
};

export default PerfilComponent;
