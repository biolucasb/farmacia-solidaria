import { listAllSaida } from "@infrastructure/redux/saida";
import { AppDispatch, RootState } from "@infrastructure/redux/store";
import { TipoMovimentacao } from "@infrastructure/types/base.enums";
import { SelectProps, TableProps } from "antd";
import { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const useRelatorios = () => {
  const [tipoMovimentacao, setTipoMovimentacao] = useState<TipoMovimentacao>(
    TipoMovimentacao.AMBOS
  );
  const [query, setQuery] = useState<string | undefined>(undefined);
  const dispatch = useDispatch<AppDispatch>();
  const { list, listLoading } = useSelector((state: RootState) => state.saida);
  useEffect(() => {
    dispatch(listAllSaida({ pageSize: 10, pageIndex: 0 }));
  }, [dispatch]);

  const dataSource = useMemo(() => {
    return list.map((item) => {
      return {
        key: item.id,
        data: new Date(item.dataRetirada)?.toLocaleDateString() || "",
        tipo: item.tipo === TipoMovimentacao.DOACAO ? "Doação" : "Saída",
        quantidade: item.quantidade,
        medicamento: `${item.medicamento.nomeComercial} - ${item.medicamento.principioAtivo} - ${item.medicamento.concentracao}`,
        observacao: item.observacao ?? "Não informado",
        doadoRetiradoPor: item.retirante.nome,
      };
    });
  }, [list]);

  const columns = useMemo(() => {
    return [
      {
        title: "Data",
        dataIndex: "data",
        key: "data",
      },
      {
        title: "Tipo",
        dataIndex: "tipo",
        key: "tipo",
      },
      {
        title: "Doado/Retirado por",
        dataIndex: "doadoRetiradoPor",
        key: "doadoRetiradoPor",
      },
      {
        title: "Quantidade",
        dataIndex: "quantidade",
        key: "quantidade",
      },
      {
        title: "Medicamento",
        dataIndex: "medicamento",
        key: "medicamento",
      },
      {
        title: "Observação",
        dataIndex: "observacao",
        key: "observacao",
      },
    ];
  }, []);
  const tableProps: TableProps = {
    dataSource,
    columns,
  };

  const selectOptions: SelectProps = {
    value: tipoMovimentacao,
    onChange: (value: TipoMovimentacao) => {
      setTipoMovimentacao(value);
      dispatch(
        listAllSaida({
          pageIndex: 0,
          pageSize: 10,
          query: query,
          tipo: value !== TipoMovimentacao.AMBOS ? value : undefined,
        })
      );
    },
    defaultValue: TipoMovimentacao.AMBOS,
    options: [
      { label: "Todos", value: TipoMovimentacao.AMBOS },
      { label: "Doação", value: TipoMovimentacao.DOACAO },
      { label: "Saída", value: TipoMovimentacao.SAIDA },
    ],
  };

  const onSearch = (value: string) => {
    setQuery(value);
    dispatch(
      listAllSaida({
        pageIndex: 0,
        pageSize: 10,
        query: value,
        tipo:
          tipoMovimentacao !== TipoMovimentacao.AMBOS
            ? tipoMovimentacao
            : undefined,
      })
    );
  };
  return {
    tableProps,
    tipoMovimentacao,
    setTipoMovimentacao,
    listLoading,
    onSearch,
    selectOptions,
  };
};
export default useRelatorios;
