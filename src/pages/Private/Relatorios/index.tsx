import { Col, Flex, Row, Table, Input, Select } from "antd";
import useRelatorios from "./hooks";
import Loading from "@components/Loading";
import { FaSearch } from "react-icons/fa";

const { Search } = Input;
const RelatoriosPage: React.FC = () => {
  const { tableProps, listLoading, onSearch, selectOptions } = useRelatorios();

  return (
    <div className="centralizar-botoes pt-2" style={{ width: "95%" }}>
      <Flex
        vertical
        align="center"
        justify="center"
        gap={5}
        style={{ padding: "2%", width: "100%" }}
      >
        <Search
          placeholder="Buscar"
          enterButton={<FaSearch />}
          onChange={(e) => onSearch(e.target.value)}
          style={{ width: "50%" }}
        />
        <Select {...selectOptions} />
      </Flex>
      <Row align={"middle"} justify={"center"}>
        <Col span={20}>
          <h1>Movimentações</h1>
        </Col>
      </Row>
      <Row align={"middle"} justify={"center"}>
        <Col span={20}>
          {listLoading ? <Loading /> : <Table {...tableProps} />}
        </Col>
      </Row>
    </div>
  );
};

export default RelatoriosPage;
