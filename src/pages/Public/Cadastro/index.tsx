import React from 'react';
import FormCadastrar from "@components/FormMedicamento";
import FormCliente from "@components/FormCliente";
import Header from "@components/Header";
import FormSaida from "@components/FormSaida";
import RegistroUser from "@components/CadUser";
import { Layout, Row, Col } from 'antd';
import './style.css';

const { Content } = Layout;

const CadastroComponent: React.FC = () => {
  return (
    <Layout>
      <Header />
      <Content>
        <div className="centralizar">
          <Row gutter={[16, 16]}>
            <Col className='pt-5' xs={24} sm={24} md={12} lg={12} xl={12}>
              <FormCadastrar />
            </Col>
            <Col className='pt-5' xs={24} sm={24} md={12} lg={12} xl={12}>
              <FormCliente />
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col className='pt-5' xs={24} sm={24} md={12} lg={12} xl={12}>
              <FormSaida />
            </Col>
            <Col className='pt-5' xs={24} sm={24} md={12} lg={12} xl={12}>
              <RegistroUser />
            </Col>
          </Row>
        </div>
      </Content>
      
    </Layout>
  );
};

export default CadastroComponent;
