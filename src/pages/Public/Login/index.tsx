import React, { useEffect } from "react";
import { Form, Input, Button, Typography } from "antd";
import Logo from "../../../assets/logo.png";
import "./style.css";
import { useAuth } from "@services/auth";
import { useNavigate } from "react-router-dom";

const { Text } = Typography;

const LoginComponent: React.FC = () => {
  const [form] = Form.useForm();
  const { login, logged } = useAuth();
  const navigate = useNavigate();
  const handleLogin = () => {
    form.validateFields().then((data) => {
      login(data.username, data.password);
    });
  };

  useEffect(() => {
    if (logged) {
      navigate("/");
    }
  }, [logged]);

  return (
    <div className="container padding-center d-flex">
      <div className="centralizar">
        <div className="justify-content-center pl-3">
          <img className="imagem" src={Logo} alt="" />
          <div className="flex-column pt-4">
            <Form form={form} name="login" className="centralizar2">
              <Form.Item
                style={{ width: "75%" }}
                name="username"
                rules={[
                  {
                    required: true,
                    message: "Por favor insira o seu email!",
                  },
                  {
                    type: "email",
                    message: "O email deve ser um email válido",
                  },
                ]}
              >
                <Input placeholder="Email" type="email" />
              </Form.Item>

              <Form.Item
                style={{ width: "75%" }}
                name="password"
                rules={[
                  { required: true, message: "Por favor insira a sua senha!" },
                ]}
              >
                <Input.Password placeholder="Senha" />
              </Form.Item>

              <Form.Item>
                <Text type="secondary">
                  Se você não possui o usuário por favor contate o fabricante.
                </Text>
              </Form.Item>

              <Form.Item>
                <Button
                  type="primary"
                  className="col-lg-24 text-center"
                  onClick={handleLogin}
                >
                  Entrar
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginComponent;
