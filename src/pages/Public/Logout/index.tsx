import { useAuth } from "@services/auth";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const LogoutComponent: React.FC = () => {
  const { logout } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    logout();
    navigate("/");
  }, [logout, navigate]);
  
  return (
    <div>
      <h1>Logout</h1>
    </div>
  );
};

export default LogoutComponent;
