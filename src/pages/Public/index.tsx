export { default as LoginComponent } from "./Login";
export { default as LogoutComponent } from "./Logout";
export { default as CadastroComponent } from "./Cadastro";
export { default as RecuperarSenhaComponent } from "./RecuperarSenha";
