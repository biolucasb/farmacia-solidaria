import {
  AuthContextProviderProps,
  AuthContextType,
} from "@infrastructure/types";
import React from "react";
import useAuthService from "./hooks";

const AuthContext = React.createContext<AuthContextType | undefined>(undefined);

export const AuthContextProvider: React.FC<AuthContextProviderProps> = ({
  children,
}) => {
  const { fblogin, fblogout, logged, user, loading, access_token, isAdmin } =
    useAuthService();
  const login = async (email: string, password: string) => {
    try {
      const user = await fblogin(email, password);
      if (user) {
        localStorage.setItem(
          "user",
          JSON.stringify({
            token: await user.getIdToken(),
            email: user.email,
            refreshToken: user.refreshToken,
            admin: isAdmin,
          })
        );
      }
    } catch (e) {
      console.error(e);
    }
  };

  const logout = async () => {
    try {
      await fblogout();
      localStorage.removeItem("user");
    } catch (e) {
      console.error(e);
    }
  };

  const contextValue: AuthContextType = {
    user,
    logged,
    access_token,
    loading,
    isAdmin,
    login,
    logout,
  };
  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  );
};

export default AuthContext;
