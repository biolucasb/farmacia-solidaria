import { initializeApp } from "firebase/app";
import {
  getAuth,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  updateCurrentUser,
  User,
} from "firebase/auth";
import { useEffect, useState } from "react";
const useAuthService = () => {
  const app = initializeApp({
    apiKey: import.meta.env.VITE_FIREBASE_API_KEY,
    authDomain: import.meta.env.VITE_FIREBASE_PROJECT_ID + ".firebaseapp.com",
    projectId: import.meta.env.VITE_FIREBASE_PROJECT_ID,
    storageBucket: import.meta.env.VITE_FIREBASE_PROJECT_ID + ".appspot.com",
    messagingSenderId: import.meta.env.VITE_FIREBASE_MESSAGING_SENDER_ID,
    appId: import.meta.env.VITE_FIREBASE_APP_ID,
  });
  const [user, setUser] = useState<User | null | undefined>(undefined);
  const [access_token, setAccess_token] = useState<string | null>(null);
  const [isAdmin, setIsAdmin] = useState<boolean>(false);
  const auth = getAuth(app);
  useEffect(() => {
    if (user) {
      user.getIdTokenResult().then((tokenResult) => {
        if (Date.parse(tokenResult.expirationTime) < Date.now()) return;
        user.reload();
      });
    }
    const unsubscribe = auth.onAuthStateChanged(async (user) => {
      setUser(user);
      const tokenResult = await user?.getIdTokenResult();
      setAccess_token(tokenResult?.token || null);
      setIsAdmin((tokenResult?.claims.admin as boolean) || false);
      localStorage.setItem(
        "user",
        JSON.stringify({
          token: tokenResult?.token,
          email: user?.email,
          refreshToken: user?.refreshToken,
          admin: tokenResult?.claims.admin,
        })
      );
    });
    return () => unsubscribe();
  }, [auth, user]);

  const fblogin = async (
    email: string,
    password: string
  ): Promise<User | null> => {
    const response = await signInWithEmailAndPassword(auth, email, password);
    if (response.user) {
      updateCurrentUser(auth, response.user);
      setUser(response.user);
      setAccess_token(await response.user.getIdToken());
    }
    return response.user;
  };
  const fbregister = (email: string, password: string) => {
    return createUserWithEmailAndPassword(auth, email, password);
  };
  const fblogout = () => {
    setUser(null);
    setAccess_token(null);
    setIsAdmin(false);
    return auth.signOut();
  };
  return {
    fblogin,
    fbregister,
    fblogout,
    userIsCarregando: user === undefined,
    logged: user !== null && user !== undefined,
    loading: user === undefined,
    access_token,
    isAdmin,
    user,
  };
};
export default useAuthService;
