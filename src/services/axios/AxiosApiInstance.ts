import {
  PatternError,
  PatternValidationError,
  ToastType,
} from "@infrastructure/types";
import { showToast } from "@utils/toast";
import Axios from "axios";

const URI = import.meta.env.VITE_API_URL;

const axios = Axios.create({
  baseURL: URI,
});

axios.interceptors.request.use((config) => {
  const user = localStorage.getItem("user");
  const { token } = user ? JSON.parse(user) : { token: null };
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    const { status } = error.response;
    if (status === 400) {
      const errorResponse = error.response.data as PatternError;
      if (errorResponse.message && errorResponse.code) {
        showToast(errorResponse.message, ToastType.ERROR);
      }
    } else if (status === 422) {
      const errorResponse =
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        error.response.data as PatternValidationError<any>;

      for (const key in errorResponse.errors) {
        if (errorResponse.errors[key]) {
          errorResponse.errors[key].map((error: string) =>
            showToast(error, ToastType.ERROR)
          );
        }
      }
    }
    return Promise.reject(error);
  }
);

export default axios;
