import { IMask } from "antd-mask-input";

export const cnpjMask = (cnpj: string): string => {
  if (!cnpj) return cnpj;
  const mask = IMask.createMask({
    mask: "00.000.000/0000-00",
  });
  return mask.resolve(cnpj);
};

export const cnpjUnmask = (cnpj: string): string => {
  if (!cnpj) return cnpj;
  const mask = IMask.createMask({
    mask: "00000000000000",
  });
  return mask.resolve(cnpj);
};
