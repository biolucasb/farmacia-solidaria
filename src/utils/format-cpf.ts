import { IMask } from 'antd-mask-input';

export default function CPFformat(cpf: string): string {
  const mask = IMask.createMask({
    mask:"000.000.000-00"
  })

  return mask.resolve(cpf);
}