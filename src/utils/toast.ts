import { ToastType } from "@infrastructure/types";
import { toast } from "react-toastify";

export const showToast = (
  message: string,
  toastType: ToastType = ToastType.INFO
) => {
  toast(message, {
    position: "top-right",
    autoClose: 3000,
    hideProgressBar: true,
    closeOnClick: true,
    type: toastType,
  });
};
